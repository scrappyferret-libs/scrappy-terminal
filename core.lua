--- Required libraries.

-- Localised functions.
local insert = table.insert
local remove = table.remove
local gsub = string.gsub

-- Helper functions.
local function unpack( t, i )
	i = i or 1
	if t[i] ~= nil then
		return t[i], unpack(t, i + 1)
	end
end

-- Localised values.

--- Class creation.
local library = {}

function library:init( params )

	-- Set up the params table
	self._params = params or {}

	-- Store out the params
	self._historySize = self._params.historySize or 5
	self._toggleKey = self._params.toggleKey or "tab"
	self._textSize = self._params.textSize or 10
	self._size = self._params.size or 0.35

	-- Table for history entries
	self._historyEntries = {}

	-- Table for registered commands
	self._commands = {}

	local help = function( name )

		-- Was a command name passed in?
		if name then

			-- Get the command
			local command = self:getCommand( name )

			-- Do we have one?
			if command then

				-- Then print its description
				self:print( command.description or "No description for ' " .. name .. "' found." )

			else -- No command found

				-- So tell the user.
				self:print( "'" .. name .. "'" .. " command not found, to see a list of valid commands simply enter 'help'." )

			end

		else

			-- Initial string for command list
			local commands = "Valid commands: "

			-- Loop through each command
			for k, _ in pairs( self._commands ) do

				-- Appending the name
				commands = commands .. k .. ", "

			end

			-- And then display the list
			self:print( commands:sub( 1, #commands - 2 ) )

		end

	end

	local run = function( name, ... )
		self:print( "Running command: " .. name )
	end

	--self:registerCommand( "help", help )
	self:registerCommand( "run", run, "Runs a batch file. Usage: 'run batchFileName' arg1 arg2 " )

	self:registerCommand( "clear", function() self:clear() end, "Clears the terminal history." )
	self:registerCommand( "close", function( instant ) self:hide( instant ) end, "Closes the terminal." )
	self:registerCommand( "hide", function( instant ) self:hide( instant ) end, "Closes the terminal." )
	self:registerCommand( "print", function( ... ) self:print( ... ) end, "Prints to the terminal. Usage: 'print \"Hello, world!\" \"something else\" 14'" )

	-- Add the key event listener
	Runtime:addEventListener( "key", self )

end

--- Registers a command.
-- @param name The name of the command.
-- @param callback The callback/listener for the command.
-- @param description The description of the command, displayed when using 'help'.
function library:registerCommand( name, callback, description )
	self._commands[ name ] = { callback = callback, description = description }
end

--- Gets a command.
-- @param name The name of the command.
-- @return The command.
function library:getCommand( name )
	return self._commands[ name ]
end

--- Checks if a command exists.
-- @param name The name of the command.
-- @return True if it does, false otherwise.
function library:hasCommand( name )
	return self:getCommand( name ) ~= nil
end

--- Shows the terminal.
-- @param instant True if the terminal should be shown instantly. Optional, defaults to false.
function library:show( instant )

	-- Make sure we're not disabled
	if not self:isDisabled() then

		-- Make sure we don't already have a transition or this is instant
		if not self._transition or instant then

			-- Cancel the transition
			if self._transition then
				transition.cancel( self._transition )
				self._transition = nil
			end

			-- Create the visual
			self:_createVisual()

			-- On complete handler for the showing
			local onComplete = function()

				-- Mark us as visible
				self._isVisible = true

				-- Cancel the transition
				if self._transition then
					transition.cancel( self._transition )
					self._transition = nil
				end

				-- Set the keyboard focus
				native.setKeyboardFocus( self._input )

			end

			-- Should this be instant?
			if instant then

				-- Then just call the onComplete handler
				onComplete()

			else -- Otherwise

				-- Do a transition
				self._transition = transition.from( self._visual, { time = 250, y = -self._visual.contentHeight, transition = easing.outQuad, onComplete = onComplete } )

			end

		end

	end

end

--- Hides the terminal.
-- @param instant True if the terminal should be hidden instantly. Optional, defaults to false.
function library:hide( instant )

	-- Make sure we don't already have a transition or this is instant
	if not self._transition or instant then

		-- Cancel the transition
		if self._transition then
			transition.cancel( self._transition )
			self._transition = nil
		end

		-- Set the keyboard focus to nil
		native.setKeyboardFocus( nil )

		-- On complete handler for the showing
		local onComplete = function()

			-- Mark us as not visible
			self._isVisible = false

			-- Cancel the transition
			if self._transition then
				transition.cancel( self._transition )
				self._transition = nil
			end

			-- Destroy the visual
			self:_destroyVisual()

		end

		-- Should this be instant?
		if instant then

			-- Then just call the onComplete handler
			onComplete()

		else -- Otherwise

			-- Do a transition
			self._transition = transition.to( self._visual, { time = 250, y = -self._visual.contentHeight, transition = easing.outQuad, onComplete = onComplete } )

		end

	end

end

--- Shows/hides the terminal.
-- @param instant True if the terminal should be toggled instantly. Optional, defaults to false.
function library:toggle( instant )
	if self:isVisible() then
		self:hide( instant )
	else
		self:show( instant )
	end
end

--- Checks if a the terminal is currently visible.
-- @return True if it is, false otherwise.
function library:isVisible()
	return self._isVisible
end

--- Enables the terminal.
function library:enable()
	self._isDisabled = false
end

--- Disables the terminal.
function library:disable()
	self._isDisabled = true
	self:hide( true )
end

--- Checks if a the terminal is currently disabled.
-- @return True if it is, false otherwise.
function library:isDisabled()
	return self._isDisabled
end

--- Clear the terminal history.
function library:clear()

	-- Clear the history text
	self._history.text = ""

	-- Clear the history entries
	self._historyEntries = {}

	-- And finally adjust the history text position
	self._history.y = ( self._output.y - self._output.contentHeight * 0.5 ) - self._history.contentHeight * 0.5

end

--- Prints to the terminal.
-- @param ... The arguments to print.
function library:print( ... )

	-- Convert the var args to a table
	local t = { ... }

	-- Set the base text as the first arg
	local line = t[ 1 ]

	-- Loop through the args
	for i = 2, #( t or {} ), 1 do

		-- Append the text
		line = line .. " 	 " .. t[ i ]

	end

	-- Add the entry to the history
	self:_addEntryToHistory( line )

end

--- Listener for the user input event.
-- @param event The event table.
function library:userInput( event )

	-- Remove the toggle key
	local text = gsub( self._input.text, self._toggleKey, "" )

	-- Was this just submitted?
	if event.phase == "submitted" then

		-- Add the entry to the history
		self:_addEntryToHistory( text )

		-- Parse the command
		local command, args = self:_parseCommand( text )

		-- Now run the command
		self:_runCommand( command, unpack( args or {} ) )

		-- Clear the input text
		self._input.text = ""

		-- Clear the output text
		self._output.text = ""

	else -- Otherwise

		-- Update the output text
		self._output.text = text

	end

end

--- Listener for the key event.
-- @param event The event table.
function library:key( event )

	-- Was the toggle key just pressed?
	if event.phase == "up" and event.keyName == self._toggleKey then

		-- Them toggle us!
		self:toggle()

	end

end

--- Listener for the enterFrame event.
-- @param event The event table.
function library:update( event )

	-- Do we have a valid visual?
	if self._visual and self._visual[ "toFront" ] then

		-- Send it to the front!
		self._visual:toFront()

	end

end

--- Create the main visual.
function library:_createVisual()

	-- Destroy the existing visual, if any
	self:_destroyVisual()

	-- Create the main visual group
	self._visual = display.newGroup()

	-- Create the main back bar
	self._back = display.newRect( self._visual, 0, 0, display.contentWidth, display.contentHeight * self._size )

	-- And colour it
	self._back:setFillColor( 0, 0, 0, 0.75 )

	-- Create the input text field
	self._input = native.newTextField( 0, display.contentHeight * 2, self._back.contentWidth, self._textSize )
	self._input:addEventListener( "userInput", self )

	-- And hide it off screen
	self._input.x = display.contentCenterX
	self._input.y = display.contentHeight * 2

	-- Options for all output text
	local options =
	{
	    text = "",
	    x = 0,
	    width = self._back.contentWidth * 0.95,
	    font = native.systemFont,
	    fontSize = self._input.contentHeight,
	    align = "left"
	}

	-- Create the displayed output text
	self._output = display.newText( options )
	self._output.y = self._back.y + self._back.contentHeight * 0.5 - self._output.contentHeight * 0.5
	self._visual:insert( self._output )

	-- And the history text
	self._history = display.newText( options )
	self._history.entries = {}
	self._visual:insert( self._history )

	-- Fade the history out a bit
	self._history.alpha = 0.5

	-- Position the terminal
	self._visual.x = display.contentCenterX
	self._visual.y = self._visual.contentHeight * 0.5

	-- And set the keyboard focus
	timer.performWithDelay( 1, function() native.setKeyboardFocus( self._input ) end, 1 )

	for i = 1, #self._historyEntries, 1 do
		self:_addEntryToHistory( self._historyEntries[ i ], true )
	end

	-- Make sure the terminal has focus when you touch it
	local onTouch = function( event )
		native.setKeyboardFocus( self._input )
	end
	self._back:addEventListener( "touch", self )

end

--- Destroy the main visual.
function library:_destroyVisual()

	-- Destroy the visual
	display.remove( self._visual )
	self._visual = nil

end

--- Parses a command.
-- @param line The entered line.
-- @return The name of the command and a table containing any arguments.
function library:_parseCommand( line )

	-- Table to store the args
	local args = {}

	-- Loop through the line splitting it into words ( or sentences if in quotes ) - borrowed from here: https://stackoverflow.com/questions/28664139/lua-split-string-into-words-unless-quoted
	local spat, epat, buf, quoted = [=[^(['"])]=], [=[(['"])$]=]
	for str in line:gmatch("%S+") do
	  local squoted = str:match(spat)
	  local equoted = str:match(epat)
	  local escaped = str:match([=[(\*)['"]$]=])
	  if squoted and not quoted and not equoted then
	    buf, quoted = str, squoted
	  elseif buf and equoted == quoted and #escaped % 2 == 0 then
	    str, buf, quoted = buf .. ' ' .. str, nil, nil
	  elseif buf then
	    buf = buf .. ' ' .. str
	  end
	  if not buf then  args[ #args + 1 ] = (str:gsub(spat,""):gsub(epat,"")) end
	end
	if buf then print("Missing matching quote for "..buf) end

	-- Get the command name
	local name = args[ 1 ]

	-- And remove it from the args
	remove( args, 1 )

	for i = 1, #args, 1 do

		-- Convert the args to useful things if required
		if args[ i ] == "true" then
			args[ i ] = true
		elseif args[ i ] == "false" then
			args[ i ] = false
		elseif args[ i ] == "nil" then
			args[ i ] = nil
		end

	end

	-- Return the command name and the args
	return name, args

end

--- Runs a command.
-- @param command The name of the command, or the callback itself.
-- @param ... The arguments.
function library:_runCommand( command, ... )

	-- Was the passed in command actually a name?
	if command and type( command ) == "string" and self:hasCommand( command ) then

		-- Get the actual command
		command = self:getCommand( command ).callback

	end

	-- Do we have a valid command?
	if command and type( command ) == "function" then

		-- Now call the command, passing in the args
		command( ... )

	end

end

--- Places an entry into the terminal history.
-- @param entry The entry to add.
-- @param justDisplay If true then this entry won't actually be properly logged, just shown. Optional, defaults to false.
function library:_addEntryToHistory( entry, justDisplay )

	-- Is this entry not just for display?
	if not justDisplay then

		-- Add this entry to the history
		self._historyEntries[ #self._historyEntries + 1 ] = entry

	end

	-- Do we have more entries in the history than we should have?
	if #self._historyEntries > self._historySize then

		-- Then remove the first one
		remove( self._historyEntries, 1 )

	end

	-- Do we have a visual for the history?
	if self._history and self._history[ "toFront" ] and type( self._history[ "toFront" ] ) == "function" then

		-- Clear the history text
		self._history.text = ""

		-- Loop through each history entry
		for i = 1, #self._historyEntries, 1 do

			-- And append it to the text as a new line
			self._history.text = self._history.text .. "\n" .. self._historyEntries[ i ]

		end

		-- And finally adjust the history text position
		self._history.y = ( self._output.y - self._output.contentHeight * 0.5 ) - self._history.contentHeight * 0.5

	end

end

--- Destroy the terminal.
function library:destroy()

	-- Cancel the transition
	if self._transition then
		transition.cancel( self._transition )
		self._transition = nil
	end

	-- Destroy our visual
	self:_destroyVisual()

	-- Remove the key event listener
	Runtime:removeEventListener( "key", self )

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Terminal library
if not Scrappy.Terminal then

	-- Then store the library out
	Scrappy.Terminal = library

end

-- Return the new library
return library
